# 💻 Exercício-Programa #2

Repositório destinado à entrega do segundo exercício-programa da disciplina **MAC0475 - Laboratório de Sistemas Computacionais Complexos**. Cuja implementação abrange as atividades de registro e login, com preenchimento de formulários reativos e etapas de autenticação e autorização, tudo sob a perspectiva do *frontend*.

# 📦 Dependências

* [NuxtJS](https://nuxtjs.org/)
* [VueJS](https://vuejs.org/)
* [Axios](https://axios.nuxtjs.org/)
* [TailwindCSS](https://tailwindcss.com/)
* [JSON Server Auth](https://www.npmjs.com/package/json-server-auth)

# ⚙️ Instalação

```
npx create-nuxt-app <project-name>
```

***

Configurações do projeto (escolhidas interativamente no terminal):

* **Project name:** `<project-name>`
* **Programming language:** `JavaScript`
* **Package manager:** `Npm`
* **UI framework:** `TailwindCSS`
* **Nuxt.js modules:** `Axios - Promise based HTTP client`
* **Linting tools:** `ESLint`
* **Testing framework:** `None`
* **Rendering mode:** `Universal (SSR / SSG)`
* **Deployment target:** `Static (Static/Jamstack hosting)`
* **Development tools:** `jsconfig.json (Recomended for VS Code if you're not using typescript)`
* **Continuous integration:** `None`
* **Version control system:** `Git`

***

```
cd <project-name>
npm install axios json-server json-server-auth
```

# 🚀 Uso

```
npx json-server-auth db.json 
npm run dev
```

# ✨ Exemplo

![example-gif](https://i.ibb.co/sjt5Bcx/example.gif)
