export const state = () => ({
    errors : {
        'username':'Required field',
        'password':'Required field',
        'email':'Required field'
    }
})
    
export const mutations = {    
    add(state, { err, key }) {
        state.errors[key] = err
    },

    clear(state, _) {
        state.errors['username'] = 'Required field'
        state.errors['password'] = 'Required field'
        state.errors['email'] = 'Required field'
    },
}