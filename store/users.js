export const state = () => ({
    data : {
        'username':'',
        'password':'',
        'email':''
    }
})
    
export const mutations = {    
    add(state, { dt, key }) {
        state.data[key] = dt
    },

    clear(state, _) {
        state.data['username'] = ''
        state.data['password'] = ''
        state.data['email'] = ''
    },
}